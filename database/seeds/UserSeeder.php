<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Ali Khalili',
            'email' => 'alikhalili4549@gmail.com',
            'password' => Hash::make('123456'),
        ]);
    }
}
