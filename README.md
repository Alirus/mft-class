<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="100%"></p>

## About This Repository

<p align="right">این مخزن برای کلاس لاراول مجتمع فنی تهران ساخته شده است</p>

## Help

<p align="right" dir="rtl">برای مشاهده قسمت های زیر باید در برنچ ها جستجو کنید:</p>

Ch-01 : multiple language website (وبسایت چند زبانه)

## Contributing

<p align="right">هیچ مشارکت کننده ای وجود ندارد</p>

## License

<p align="right">پروژه اوپن سورس است</p>
